# Напишите функцию которая принемает *args
war = [10, 20, 55, 8, 90]
def print1(*args):
    print(args)
    print(sum(args))

print1(223, 54, 3)

# Напишите функцию которая принемает *kwargs
def printkwargs(**kwargs):
    print(kwargs)

printkwargs(home=6, lesson=7, end=8)

# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
def numb(*args):
    for i in args:
        print(i)
numb(7, 8, 'o', war)
# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием параметров args, kwargs


hero_undead =[
    {'name': 'artes',
     'kills': 23
     },
    {
     'name': 'lich',
    'kills': 20
    }
]

hero_orc = [
    {'name': 'trall',
     'kills': 22
     },
    {'name': 'garrosh',
     'kills': 45
     }
]

hero_hum = [
    {'name': 'uter',
     'kills': 0
     },
    {'name': 'anduin',
     'kills': 0
     }
]

hero_ne = [
    {'name': 'illidan',
     'kills': 12
     },
    {'name': 'maev',
     'kills': 3}
]

def filtr(*args):
    good_hero = []
    for i in args:
        for hero in i:
         if hero['kills']<= 0:
            good_hero.append(hero)

    return good_hero

gg = filtr(hero_ne, hero_hum, hero_orc, hero_undead)

def addElement(arr):
    for i in arr:
        i["titul"] = "good hero"
    return arr

arrs =addElement(gg)
print(arrs)